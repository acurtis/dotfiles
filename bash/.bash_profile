#  ---------------------------------------------------------------------------
#
#  Description:  This file holds all my BASH configurations and aliases
#
#  Sections:
#  1.   Environment Configuration
#  2.   Make Terminal Better (remapping defaults and adding functionality)
#  3.   File and Folder Management
#  4.   Searching
#  5.   Process Management
#  6.   Networking
#  7.   System Operations & Information
#  8.   Web Development
#  9.   Reminders & Notes
#
#  ---------------------------------------------------------------------------

#   -------------------------------
#   1.  ENVIRONMENT CONFIGURATION
#   -------------------------------

#   colors
#   ------------------------------------------------------------
F_BLK="\033[30m"
F_RED="\033[31m"
F_GRN="\033[32m"
F_YLW="\033[33m"
F_BLU="\033[34m"
F_MAG="\033[35m"
F_CYN="\033[36m"
F_WHT="\033[37m"
B_BLK="\033[40m"
B_RED="\033[41m"
B_GRN="\033[42m"
B_YLW="\033[43m"
B_BLU="\033[44m"
B_MAG="\033[45m"
B_CYN="\033[46m"
B_WHT="\033[47m"

T_BLD="\033[1m"
T_DIM="\033[2m"
T_REV="\033[7m"
T_HID="\033[8m"
T_RST="\033[0m"

#   Set shell options
#   ------------------------------------------------------------
shopt -s cdspell

#   Change Prompt
#   ------------------------------------------------------------
    export PS1="$T_RST${F_GRN}\u$F_WHT@$F_GRN\h$F_YLW \D{%m/%d/%Y %H:%M:%S}\n$F_CYN\w$T_RST\n(\#) \$ "
    export PS2="_\$"

#   xtitle
#   ------------------------------------------------------------
function xtitle ( )
{
    if [ $# -eq 0 ]
    then
        title=`pwd`
    else
	title=$*
    fi

    case $TERM in
        *term | xterm | xterm-color | xterm-256color | rxvt | vt100 | gnome* )
            echo -n -e "\033]0;$title\007" ;;
        *)  ;;
    esac
}

    export PROMPT_COMMAND="xtitle"

#   Set Paths
#   ------------------------------------------------------------
    export PATH="/Users/acurtis/bin:/usr/local/bin:$PATH:/usr/local/sbin"

#   Set Default Editor (change 'Nano' to the editor of your choice)
#   ------------------------------------------------------------
    export EDITOR=/usr/bin/vim

#   Set default blocksize for ls, df, du
#   from this: http://hints.macworld.com/comment.php?mode=view&cid=24491
#   ------------------------------------------------------------
    export BLOCKSIZE=1k


#   -----------------------------
#   2.  MAKE TERMINAL BETTER
#   -----------------------------

alias mkdir='mkdir -pv'                     # Preferred 'mkdir' implementation
alias ls='ls -G'
alias ll='ls -FGlAhp'                       # Preferred 'ls' implementation
alias less='less -FSRXc'                    # Preferred 'less' implementation
alias cd..='cd ../'                         # Go back 1 directory level (for fast typers)
alias ..='cd ../'                           # Go back 1 directory level
alias ...='cd ../../'                       # Go back 2 directory levels
alias .3='cd ../../../'                     # Go back 3 directory levels
alias .4='cd ../../../../'                  # Go back 4 directory levels
alias .5='cd ../../../../../'               # Go back 5 directory levels
alias .6='cd ../../../../../../'            # Go back 6 directory levels
alias ~="cd ~"                              # ~:            Go Home
alias c='clear'                             # c:            Clear terminal display
alias path='echo -e ${PATH//:/\\n}'         # path:         Echo all executable Paths
alias show_options='shopt'                  # Show_options: display bash options settings
alias fix_stty='stty sane'                  # fix_stty:     Restore terminal settings when screwed up
alias cic='set completion-ignore-case On'   # cic:          Make tab-completion case-insensitive

#   mans:   Search manpage given in agument '1' for term given in argument '2' (case insensitive)
#           displays paginated result with colored search terms and two lines surrounding each hit.             Example: mans mplayer codec
#   --------------------------------------------------------------------
    mans () {
        man $1 | grep -iC2 --color=always $2 | less
    }

#   showa: to remind yourself of an alias (given some part of it)
#   ------------------------------------------------------------
    showa () { /usr/bin/grep --color=always -i -a1 $@ ~/Library/init/bash/aliases.bash | grep -v '^\s*$' | less -FSRXc ; }


#   -------------------------------
#   3.  FILE AND FOLDER MANAGEMENT
#   -------------------------------



#   ---------------------------
#   4.  SEARCHING
#   ---------------------------

alias qfind="find . -name "                 # qfind:    Quickly search for file
ff () { /usr/bin/find . -name "$@" ; }      # ff:       Find file under the current directory
ffs () { /usr/bin/find . -name "$@"'*' ; }  # ffs:      Find file whose name starts with a given string
ffe () { /usr/bin/find . -name '*'"$@" ; }  # ffe:      Find file whose name ends with a given string


#   ---------------------------
#   5.  PROCESS MANAGEMENT
#   ---------------------------


#   ---------------------------
#   6.  NETWORKING
#   ---------------------------

#   ---------------------------------------
#   7.  SYSTEMS OPERATIONS & INFORMATION
#   ---------------------------------------


#   ---------------------------------------
#   8.  WEB DEVELOPMENT
#   ---------------------------------------

httpHeaders () { /usr/bin/curl -I -L $@ ; }             # httpHeaders:      Grabs headers from web page

#   httpDebug:  Download a web page and show info on what took time
#   -------------------------------------------------------------------
    httpDebug () { /usr/bin/curl $@ -o /dev/null -w "dns: %{time_namelookup} connect: %{time_connect} pretransfer: %{time_pretransfer} starttransfer: %{time_starttransfer} total: %{time_total}\n" ; }


#   ---------------------------------------
#   9. Bash Completion
#   ---------------------------------------


#if [ -f $(brew --prefix)/etc/bash_completion ]; then
#  . $(brew --prefix)/etc/bash_completion
#fi


#   ---------------------------------------
#   10. Local config
#   ---------------------------------------
if [ -f ~/.profile_local ]; then
   . ~/.profile_local
fi
if [ -f ~/.bash_local ]; then
   . ~/.bash_local
fi


export HOMEBREW_NO_ANALYTICS=1

it2prof() { echo -e "\033]50;SetProfile=$1\a"; }

ssh() {
    if [[ $* == *"mon"* ]];
    then
        if test "$(find ~/.ssh -name id_rsa-cert.pub -mmin +120)"; then 
            echo "updating monsanto certificate";
            vault-auth;
            vault-cert;
        fi;
    fi

    if [[ $* == *"dev"* ]];
    then
        it2prof DEV;
    fi;
    if [[ $* == *"np"* ]];
    then
        it2prof NONPROD;
    fi;
    if [[ $* == *"prod"* ]];
    then
        it2prof PROD;
    fi;
    /usr/bin/ssh $*;
    it2prof Default;
}
test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"

# docker 
alias docker-clean="docker images | awk '\$1 ~ \"<none>\" { print \"docker rmi -f \",\$3 }' | sh"

alias code="code-oss"

export NVM_DIR="/Users/acurtis/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

# Setting PATH for Python 3.6
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.6/bin:${PATH}"
export PATH

weather ()
{
    if [ -z "$1" ]; then
        loc="wildwood,mo";
    else
        loc="$1";
    fi

    curl "wttr.in/$loc"
}

alias wanip="dig +short myip.opendns.com @resolver1.opendns.com"
