# Path to your oh-my-zsh installation.
export ZSH=$HOME/.dotfiles/external/ohmyzsh
# export ZDOTDIR=${HOME}/.zdotdir
# mkdir -p ${ZDOTDIR}

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="agnoster-multi"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=~/.dotfiles/zsh/custom

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git docker docker-compose copyfile python jump kubectl)
if type code >/dev/null; then 
    plugins=(${plugins} vscode); 
fi

if [ "$(uname)" = "Darwin" ]; then
    plugins=(${plugins} macos iterm2 brew)
fi

if type asdf >/dev/null; then
    plugins=(${plugins} asdf)
fi

if [ -f ~/.dotfiles/external/asdf/asdf.sh ]; then
    . ~/.dotfiles/external/asdf/asdf.sh
elif [ -f ~/.asdf/asdf.sh ]; then
    . ~/.asdf/asdf.sh
fi

source $ZSH/oh-my-zsh.sh

# User configuration

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

PATH=${HOME}/.local/bin:${PATH}:${HOME}/go/bin


# Key configs
bindkey -e
bindkey \^U backward-kill-line
bindkey \^k kill-line

if type jenv > /dev/null; then 
    eval "$(jenv init -)"
    # export JAVA_HOME=$(jenv javahome)
fi

# if type pyenv > /dev/null; then
#     eval "$(pyenv init -)"
# fi

# Notes
if [ -d ~/notes ]; then
    . ~/.dotfiles/notes/notes
fi

unset -f geogig >/dev/null 2>&1
GEOGIG=$(which geogig 2>/dev/null)
if [ -e $GEOGIG ]; then
    function geogig() {
        JAVA_HOME=$(jenv javahome) ${GEOGIG} $*
    }
fi

unset -f code >/dev/null 2>&1
VSCODE=$(which code 2>/dev/null)
if [ -e $VSCODE ]; then
    function code() {
        ${VSCODE} $*
    }
fi

unset -f podman-machine-ip >/dev/null 2>&1
PODMAN_MACHINE=$(which podman-machine 2>/dev/null)
if [ -e $PODMAN_MACHINE ]; then
    function podman-machine-ip() {
        export PODMAN_IP=$(${PODMAN_MACHINE} ssh box "ip -4 addr show eth1 |  sed -n -E 's/^.*inet ([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+).*$/\1/p'")
    }
fi

if type vim > /dev/null; then
    export EDITOR=$(which vim)
else 
    export EDITOR=$(which vi)
fi

# OSX Specific config
if [ "$(uname)" = "Darwin" ]; then
    export HOMEBREW_NO_ANALYTICS=1
fi

# if type kubectl > /dev/null; then
#     alias k=$(which kubectl)
# fi

if [ -f "${HOME}/.iterm2_shell_integration.zsh" ]; then
    . ~/.iterm2_shell_integration.zsh
    export PATH=${PATH}:$HOME/.iterm2
    # curl -L https://iterm2.com/shell_integration/`basename $SHELL` -o ~/.iterm2_shell_integration.`basename $SHELL`
fi

# Local config
if [ -f "${HOME}/.profile_local" ]; then
    . ~/.profile_local
fi

if [ -f "${HOME}/.zshrc_local" ]; then
    . ~/.zshrc_local
fi
